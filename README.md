# base-ts-utils

## Install

```bash
npm install --save @simplus/base-ts-utils
```

## Usage

### Env

Enviromnet contains utils for code environment. To start you can import the Utility class by adding the following line to your code

```typescript
import {Env} from '@simplus/base-ts-utils'

...
```

#### Env.isProduction(): boolean
Checks if the environment is in production mode (NODE_ENV=production), returns true if is set to __production__

#### Env.isDevelopment(): boolean
Checks if the environment is in development mode (NODE_ENV=development), returns true if is set to __development__

#### Env.isTest(): boolean
Checks if the environment is in test mode (NODE_ENV=test), returns true if is set to __test__

### Env.getEnvironment(): string
Get the current environment string (already trimed)

### Env.prefix(str: string): string
Add environment as prefix to the input

```typescript
process.env.NODE_ENV = 'development'
Env.prefix("test") // development_test
Env.prefix("test1") // development_test1
```
### Env.postfix(str: string): string
Add environment as postfix to the input

```typescript
process.env.NODE_ENV = 'development'
Env.prefix("test") // test_development
Env.prefix("test1") // test1_development
```

### Env.config
The configuration can accept a few parameters

key | type | default | Description
-------|-------|---------|-------------
env_key |  string | "NODE_ENV" | the environment variable name (that contains the environment value)
default_env |  string | "development" | the environment value

### Example

```bash
# .env

MY_ENV=production
```

```typescript
Env.isDeveloment() // true because default variable is "NODE_ENV" and default value is "development"
Env.config.default_env = 'test'
Env.isDeveloment() // false because default variable is "NODE_ENV" and default value is now "test"
Env.isTest() // true because default variable is "NODE_ENV" and default value is still "test"
Env.config.env_key = 'MY_ENV'
Env.isTest() // false because default variable is now "MY_ENV" and value was set to "production"
Env.isProduction() // true because default variable is now "MY_ENV" and value was set to "production"
```

## Types

The package also contains a list of satndard types usefull 

```typescript
export type Callback<T = any> = (err?: Error|null|undefined, data?: T) => void
```