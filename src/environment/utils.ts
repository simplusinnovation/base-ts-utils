export type EnvValue = 'production'|'development'|'test'|'debug'|'qa'

/**
 * Environment utilities configuration
 */
export interface EnvConfig {
	env_key: string
	default_env: EnvValue
}

/**
 * Environment utilities
 */
export class Env {
	static config: EnvConfig = {
		env_key: 'NODE_ENV',
		default_env: 'development'
	}

	/**
	 * Prefixes with environment
	 * @param str the string that needs to be prefixed
	 */
	static prefix(str: string): string {
		return `${this.getEnvironment()}_${str}`
	}

	/**
	 * Prostfixes with environment
	 * @param str the string that needs to be postfixed
	 */
	static postfix(str: string): string {
		return `${str}_${this.getEnvironment()}`
	}

	/**
	 * Check if the environment is in production
	 */
	static isProduction(): boolean {
		return this.getEnvironment() === 'production'
	}

	/**
	 * Check if the environment is in production
	 */
	static isQa(): boolean {
		return this.getEnvironment() === 'qa'
	}

	/**
	 * Check if the environment is in test
	 */
	static isTest(): boolean {
		return this.getEnvironment() === 'test'
	}

	/**
	 * Check if the environment is in test
	 */
	static isDevelopment(): boolean {
		return this.getEnvironment() === 'development'
	}

	/**
	 * Check if the environment is in test
	 */
	static isDebug(): boolean {
		return this.getEnvironment() === 'debug'
	}

	/**
	 * Parses the environment value ("production"|"test"|"development")
	 */
	static getEnvironment(): EnvValue {
		if (!process.env[this.config.env_key]) return this.config.default_env
		return (`${process.env[this.config.env_key]}`).trim() as EnvValue || this.config.default_env
	}
}
