import dotenv = require('dotenv')
dotenv.config()

import * as mocha from 'mocha'
import {assert} from 'chai'
import {Env} from '../src'

describe('Test Env utils', () => {
	it('Test of config env_key', () => {
		assert(Env.isTest(), 'NODE_ENV should be the default env_key, should exist and be from value "test"')
		Env.config.env_key = 'TEST_ENV'
		assert(Env.isTest(), 'TEST_ENV should exist and be from value "test"')
		Env.config.env_key = 'PROD_ENV'
		assert(Env.isProduction(), 'PROD_ENV should exist and be from value "production"')
		Env.config.env_key = 'DEV_ENV'
		assert(Env.isDevelopment(), 'DEV_ENV should exist and be from value "development"')
		Env.config.env_key = 'DEBUG_ENV'
		assert(Env.isDebug(), 'DEBUG_ENV should exist and be from value "debug"')
	})

	it('Test of config default_env', () => {
		Env.config.env_key = 'UNDEFINED_ENV'
		assert(Env.isDevelopment(), 'If nothing assigned to the env_key the value should be development by default')
		Env.config.default_env = 'test'
		assert(Env.isTest(), 'The environment should now be test (after updating default_env to "test") by default')
	})

	it('Test of isTest', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(Env.isTest(), 'should return true if environment is set to "test"')
		Env.config.env_key = 'PROD_ENV'
		assert(!Env.isTest(), 'should return false if environment is set to "production"')
		Env.config.env_key = 'DEV_ENV'
		assert(!Env.isTest(), 'should return false if environment is set to "development"')
		Env.config.env_key = 'DEBUG_ENV'
		assert(!Env.isTest(), 'should return false if environment is set to "debug"')
		Env.config.env_key = 'QA_ENV'
		assert(!Env.isTest(), 'should return false if environment is set to "qa"')
	})

	it('Test of isDevelopment', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(!Env.isDevelopment(), 'should return true if environment is set to "test"')
		Env.config.env_key = 'PROD_ENV'
		assert(!Env.isDevelopment(), 'should return false if environment is set to "production"')
		Env.config.env_key = 'DEV_ENV'
		assert(Env.isDevelopment(), 'should return true if environment is set to "development"')
		Env.config.env_key = 'DEBUG_ENV'
		assert(!Env.isDevelopment(), 'should return true if environment is set to "debug"')
		Env.config.env_key = 'QA_ENV'
		assert(!Env.isDevelopment(), 'should return true if environment is set to "qa"')
	})

	it('Test of isDevelopment', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(!Env.isProduction(), 'should return true if environment is set to "test"')
		Env.config.env_key = 'PROD_ENV'
		assert(Env.isProduction(), 'should return false if environment is set to "production"')
		Env.config.env_key = 'DEV_ENV'
		assert(!Env.isProduction(), 'should return true if environment is set to "development"')
		Env.config.env_key = 'DEBUG_ENV'
		assert(!Env.isProduction(), 'should return true if environment is set to "debug"')
		Env.config.env_key = 'QA_ENV'
		assert(!Env.isProduction(), 'should return true if environment is set to "qa"')
	})

	it('Test of isDebug', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(!Env.isDebug(), 'should return true if environment is set to "test"')
		Env.config.env_key = 'DEBUG_ENV'
		assert(Env.isDebug(), 'should return true if environment is set to "debug"')
		Env.config.env_key = 'DEV_ENV'
		assert(!Env.isDebug(), 'should return true if environment is set to "development"')
		Env.config.env_key = 'PROD_ENV'
		assert(!Env.isDebug(), 'should return true if environment is set to "production"')
		Env.config.env_key = 'QA_ENV'
		assert(!Env.isDebug(), 'should return true if environment is set to "qa"')
	})

	it('Test of isQa', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(!Env.isQa(), 'should return true if environment is set to "test"')
		Env.config.env_key = 'DEBUG_ENV'
		assert(!Env.isQa(), 'should return true if environment is set to "debug"')
		Env.config.env_key = 'DEV_ENV'
		assert(!Env.isQa(), 'should return true if environment is set to "development"')
		Env.config.env_key = 'PROD_ENV'
		assert(!Env.isQa(), 'should return true if environment is set to "production"')
		Env.config.env_key = 'QA_ENV'
		assert(Env.isQa(), 'should return true if environment is set to "qa"')
	})

	it('Test of prefix', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(Env.prefix('test') === 'test_test', 'should prefix')
		Env.config.env_key = 'PROD_ENV'
		assert(Env.prefix('test') === 'production_test', 'should prefix')
		Env.config.env_key = 'DEV_ENV'
		assert(Env.prefix('test') === 'development_test', 'should prefix')
		Env.config.env_key = 'DEBUG_ENV'
		assert(Env.prefix('test') === 'debug_test', 'should prefix')
		Env.config.env_key = 'QA_ENV'
		assert(Env.prefix('test') === 'qa_test', 'should prefix')
	})

	it('Test of postfix', () => {
		Env.config.env_key = 'TEST_ENV'
		assert(Env.postfix('test') === 'test_test', 'should postfix')
		Env.config.env_key = 'PROD_ENV'
		assert(Env.postfix('test') === 'test_production', 'should postfix')
		Env.config.env_key = 'DEV_ENV'
		assert(Env.postfix('test') === 'test_development', 'should postfix')
		Env.config.env_key = 'DEBUG_ENV'
		assert(Env.postfix('test') === 'test_debug', 'should postfix')
		Env.config.env_key = 'QA_ENV'
		assert(Env.postfix('test') === 'test_qa', 'should postfix')
	})
})